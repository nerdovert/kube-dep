import { Application, Router } from "./deps.ts";

import todos from "./stub.ts";

const DEFAULT_PORT = 8080;
const port = DEFAULT_PORT;

const app = new Application();
const router = new Router();

app.use(router.routes());
app.use(router.allowedMethods());

router.get("/todos", ({ response }: { response: any }) => {
  response.body = todos;
});

console.log(`Server is running on port ${port}`);

await app.listen({ port });
